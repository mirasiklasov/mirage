package sample;

public class PiranhaFishSlayer extends WaterCreatures implements CarnivorousFish{


    public PiranhaFishSlayer(String name) {
        super(name);
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + ": Omnomnom");
    }

    @Override
    public void swim() {
        swimInAquarium();
    }

    @Override
    public void swimInAquarium() {
        System.out.println(getName() + ": this fish look so tasty." );
        eatMeat();
    }
}
