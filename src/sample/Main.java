package sample;

public class Main {

    public static void main(String[] args) {
        Aquarium firstAquarium = new Aquarium();

        firstAquarium.addMember(new GoldSmallFish("Lil Peep"));
        firstAquarium.addMember(new PiranhaFishSlayer("Drugs"));

        firstAquarium.accommodateParticipants();
    }
}
