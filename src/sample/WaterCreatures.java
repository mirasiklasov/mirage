package sample;

public abstract class WaterCreatures implements AquariumCreatures{
    private String name;

    public WaterCreatures(String name){
        setName(name);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
