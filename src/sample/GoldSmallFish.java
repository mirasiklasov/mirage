package sample;

public class GoldSmallFish extends  WaterCreatures implements HerbivorousFish{

    public GoldSmallFish(String name){
        super(name);
    }
    @Override
    public void swimInAquarium() {
        eatSeaweed();
    }

    @Override
    public void swim() {
        swimInAquarium();
    }

    @Override
    public void eatSeaweed() {
        System.out.println(getName() + " : Omnomnom, Seaweeds so tasty");
    }
}


