package sample;

import java.util.ArrayList;

public class Aquarium {
    private ArrayList <AquariumCreatures> AquariumMembers;

    public Aquarium(){
        AquariumMembers = new ArrayList<>();
    }

    public void addMember(AquariumCreatures newMember){
        AquariumMembers.add(newMember);
    }

    public void accommodateParticipants() {
        AquariumMembers.forEach(AquariumCreatures -> AquariumCreatures.swimInAquarium());
    }
}
